package main 

import (
	"fmt"
	"net/http"
)

func index_handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "\nThe server is up and running. Congratulations!!")		
}

func about_handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "\nThis is all about me!")
}

func main() {
	http.HandleFunc("/", index_handler)
	http.HandleFunc("/about", about_handler)
	http.ListenAndServe(":8000", nil) //server created on port 8000
}