package main 

import "net/http"

type person struct {
	fname string
}

func (p *person) ServeHTTP(w http.ResponseWriter, r *http.Request) { //this is a method of any person struct. If we didn't have (p *person), that would be a simple function. Here thus, any instance (not OOP conceptually though), would have a "method", known as ServeHTTP
	w.Write([]byte("First Name:" + p.fname))
}

func main() 
{
	personOne := &person{fname : "Emma"}
	http.ListenAndServe(":8000", personOne)
}