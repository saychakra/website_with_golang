package main 

import ( 
	"fmt"
	"net/http" 
)

func main() {
	mymux := http.NewServeMux() // here this is the custom mux created. In general the default one is called nil, like in the first server code. This is just created to have a more customized feel to the code!!
	mymux.HandleFunc("/", serverFunc)
	http.ListenAndServe(":8000", mymux)
}

func serverFunc(w http.ResponseWriter, r *http.Request) {
	//w.Write([]byte("Hello Kolkata!!"))
	fmt.Fprintf(w, "Hello Kolkata!!") // printed with fmt
}