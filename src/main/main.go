package main

import ( 
	"net/http"
	"io/ioutil"
	"log"
	"strings"
)

type MyHandler struct {
}

func (this *MyHandler) ServeHTTP(w http.ResponseWriter, r* http.Request) {
	path := r.URL.Path[1:] // what this basically does is that, it slices off the first character from the URL path, ie the '/' in this case
	log.Println(path)
	//path := "templates" + r.URL.Path
	data, err := ioutil.ReadFile(string(path))

	if err == nil {
		var contentType string

		if strings.HasSuffix(path,".css") {
			contentType = "css"
		} else if strings.HasSuffix(path,".html") {
			contentType = "text/html"
		} else if strings.HasSuffix(path,".js") {
			contentType = "application/js"
		} else if strings.HasSuffix(path,".jpg") {
			contentType = "image/jpg"
		} else if strings.HasSuffix(path,".png") {
			contentType = "image/png"
		} else if strings.HasSuffix(path, ".mp4") {
			contentType = "video/mp4"
		} else {
			contentType = "text/plain"
		}

		w.Header().Add("Content-Type ", contentType)
		http.ServeFile(w, r, path) // this needs to be added in the recent versions of golang. Otherwise the css will be rendered as a simple text/plain file
		w.Write(data)
	} else {
		w.WriteHeader(404)
		w.Write([]byte("404 Brother! " + http.StatusText(404)))
	}
}

func main() {
	http.Handle("/", new(MyHandler))
	http.ListenAndServe(":8000", nil)		
}	